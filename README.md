# Very Low Frequencies for satcom

This project provides an example of a satellite telecommunication scheme based on the Direct Sequence Spread Spectrum (DSSS) modulation. It simulates multiple ground station signals sent to a single satellite simultaneously (uplink process) in the presence of a jammer. The channel impairments are based on a real telecommunication scenario in the VHF band (54 -72 MHz).

---

**Table of Contents**

- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## Installation

Install Very Low Frequencies for satcom via `pip` in a new virtual environment in the current directory:

```shell
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

## Usage

The code is analyzed in the systems report. Anyone who runs this code can choose how many users are sending simultaneously, the data rate, the satellite velocity, the transmitted power, whether there is a jammer or not, which signal will be decoded at the receiver and the overall system SNR.

## License

`Very Low Frequencies for satcom` is distributed under the terms of the [AGPLv3](https://spdx.org/licenses/AGPL-3.0-or-later.html) license.
