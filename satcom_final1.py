"""Module example of a satellite reverse uplink DSSS telecommunication system.

Module name: satcom_final.py
Version: 1.0
"""

import os
import math
from math import floor
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.signal import welch, lfilter
from scipy.fft import fft, ifft
from scipy.constants import speed_of_light
from commpy.channelcoding import Trellis, conv_encode, viterbi_decode
from commpy.filters import rrcosfilter


# Function Definitions
def lfsr(poly, init_state, length):
    """
    Fibonacci Linear Feedback Shift Register (LFSR) function.

    Args:
        poly (list): List of polynomial coefficients.
        init_state (list): Initial state of the LFSR.
        length (int): Length of the output sequence.

    Returns:
        numpy.ndarray: The generated LFSR sequence.
    """
    state = init_state.copy()
    seq = []
    for _ in range(length):
        feedback = 0
        for i, s in enumerate(state):
            if poly[i + 1] == 1:
                feedback ^= s  # XOR operation
        seq.append(state[-1])  # Mask = [0, 0, 0, ........ 0, 1]
        # Update the state by shifting right and inserting the feedback bit at the beginning
        state = [feedback] + state[:-1]
    return np.array(seq)


def generate_gold_codes(poly1, init_cond1, poly2, init_cond2, frame_length, num_codes):
    """
    Generate Gold codes.

    Parameters:
    poly1 (list): The polynomial coefficients of the first LFSR.
    init_cond1 (list): The initial conditions of the first LFSR.
    poly2 (list): The polynomial coefficients of the second LFSR.
    init_cond2 (list): The initial conditions of the second LFSR.
    frame_length (int): The length of the frame.
    num_codes (int): The number of Gold codes to generate.

    Returns:
    numpy.ndarray: The generated Gold codes.
    """
    # Generate the PN sequence
    mseq1 = lfsr(poly1, init_cond1, frame_length)
    mseq2 = lfsr(poly2, init_cond2, frame_length)

    # Initialize the Gold codes matrix
    goldcodes = np.zeros((frame_length + 1, num_codes), dtype=int)

    # Generate the Gold codes
    for shift in range(num_codes):
        shiftedseq2 = np.roll(mseq2, shift)  # Circular shift
        c_goldcode = np.bitwise_xor(mseq1, shiftedseq2)  # XOR operation
        c_goldcodeextended = np.append(
            c_goldcode, 0
        )  # Extend the Gold code by appending a zero
        goldcodes[:, shift] = c_goldcodeextended  # Store the extended Gold code

    return goldcodes


def change_average_power(signal, desired_power):
    """
    Change the average power of a complex signal to the desired power.

    Parameters:
    signal (np.ndarray): Input complex signal.
    desired_power (float): Desired average power.

    Returns:
    np.ndarray: Scaled signal with the desired average power.
    """
    current_power = np.mean(np.abs(signal) ** 2)  # Calculate the current average power
    scaling_factor = np.sqrt(
        desired_power / current_power
    )  # Calculate the scaling factor
    scaled_signal = signal * scaling_factor  # Scale the signal
    return scaled_signal


def set_signal_power(sig_in, desired_power_dbm):
    """
    Set the signal power to the desired value.

    Parameters:
    sig_in (numpy array): The input signal samples.
    desired_power_dBm (float): The desired power in dBm.

    Returns:
    numpy array: The scaled signal samples.
    """
    sig_power_w = np.mean(np.abs(sig_in) ** 2)
    desired_power_w = 10 ** ((desired_power_dbm - 30) / 10)
    scaling_factor = np.sqrt(desired_power_w / sig_power_w)
    sig_out = sig_in * scaling_factor
    return sig_out


def add_frequency_offset(samples, f, freq_offset):
    """
    Add frequency offset to the given samples.

    Parameters:
    samples (np.ndarray): Input signal samples.
    f (float): Sampling frequency.
    freq_offset (float): Frequency offset to be added.

    Returns:
    np.ndarray: Signal samples with added frequency offset.
    """
    tf = np.arange(len(samples)) / f
    samples = samples * np.exp(1j * 2 * np.pi * freq_offset * tf)
    return samples


def add_awgn_noise(signal, snr_db):
    """
    AWGN channel noise addition to the signal.

    Parameters:
    signal (np.ndarray): Input complex signal.
    snr_dB (float): Signal-to-Noise Ratio in dB.

    Returns:
    np.ndarray: Signal with AWGN noise added.
    """
    signal_power = np.mean(np.abs(signal) ** 2)
    snr_linear = 10 ** (snr_db / 10)
    noise_power = signal_power / snr_linear
    sx = np.sqrt(noise_power / 2)  # half power in each part (real and imaginary)
    noise = sx * (np.random.randn(*signal.shape) + 1j * np.random.randn(*signal.shape))
    return signal + noise


def cross_correlation_calc(sig1, sig2):
    """
    Calculate the cross-correlation of two complex signals.

    Parameters:
    sig1 (np.ndarray): First input signal.
    sig2 (np.ndarray): Second input signal.

    Returns:
    np.ndarray: Cross-correlation of the input signals.
    """
    # Ensure the inputs are numpy arrays
    sig1 = np.asarray(sig1)
    sig2 = np.asarray(sig2)
    # Calculate the maximum length of the input signals
    m = max(len(sig1), len(sig2))
    maxlag = m - 1
    m2 = 2 * m
    # Perform FFT on both signals with zero-padding to length m2
    x = fft(sig1, m2)
    y = fft(sig2, m2)
    # Calculate the cross-correlation using IFFT
    c1 = ifft(x * np.conj(y))
    # Concatenate the two parts of the cross-correlation result
    sig3 = np.concatenate([c1[m2 - maxlag : m2], c1[: maxlag + 1]])
    return sig3


def modulate_qpsk(i_bits, q_bits):
    """
    Modulate bits into QPSK symbols.

    Parameters:
    i_bits (numpy array): Array of in-phase bits.
    q_bits (numpy array): Array of quadrature bits.

    Returns:
    numpy array: Array of QPSK symbols.
    """
    # Define the QPSK constellation points according to Gray coding with power 1
    constellation = np.array(
        [np.exp(1j * (np.pi / 4 + 2 * np.pi * k / 4)) for k in range(4)]
    )

    # Modulate the bits into QPSK symbols
    symbols = []
    for count, i_bit in enumerate(i_bits):
        q_bit = q_bits[count]
        if i_bit == 0 and q_bit == 0:
            symbols.append(constellation[0])
        elif i_bit == 0 and q_bit == 1:
            symbols.append(constellation[1])
        elif i_bit == 1 and q_bit == 1:
            symbols.append(constellation[2])
        else:  # i_bit == 1 and q_bit == 0
            symbols.append(constellation[3])

    return np.array(symbols)


def demodulate_qpsk(symbols_in):
    """
    Demodulate QPSK symbols into bits.

    Parameters:
    symbols_in (numpy array): Array of received QPSK symbols.

    Returns:
    numpy array: Demodulated bits.
    """
    # Define the QPSK constellation according the Gray coding with power 1
    constellation = np.array(
        [np.exp(1j * (np.pi / 4 + 2 * np.pi * k / 4)) for k in range(4)]
    )

    # Demodulated symbols
    demodulated_symbols = []
    for symbol in symbols_in:
        distances = np.abs(symbol - constellation)
        demodulated_symbols.append(np.argmin(distances))

    # Map the demodulated symbols to bits
    out_i = []
    out_q = []
    for decimal in demodulated_symbols:
        if decimal == 0:
            out_i.append(0)
            out_q.append(0)
        elif decimal == 1:
            out_i.append(0)
            out_q.append(1)
        elif decimal == 2:
            out_i.append(1)
            out_q.append(1)
        else:  # decimal == 3
            out_i.append(1)
            out_q.append(0)

    out = np.vstack((out_i, out_q))
    out = out.T.flatten()

    return np.array(out)


def create_user_signal(
    paylo_length,
    trellis_poly,
    coded_length,
    preamble_length,
    spread_factor,
    gold_code,
    rrcfilter,
    sig_power,
    num_preamble_chips,
    sps,
):
    """
    Create a user's signal.

    Parameters:
    paylo_length (int): Length of the payload.
    trellis (object): Trellis structure for convolutional encoding.
    coded_length (int): Length of the coded message.
    preamble_length (int): Length of the preamble.
    spread_factor (int): Spreading factor.
    gold_code (numpy array): Gold code for spreading.
    rrcfilter (numpy array): Root Raised Cosine filter.
    sig_power (float): Signal power in dBm.
    num_preamble_chips (int): Number of preamble chips.
    sps (int): Samples per symbol.

    Returns:
    tuple: A tuple containing txsamples, preambleqpsk, chips_seq, perm_key,
    payload_data, interleaved_message
    """
    # Generate random payload
    payload_data = np.random.randint(0, 2, (paylo_length))

    # Perform convolutional encoding
    message = conv_encode(payload_data.flatten(), trellis_poly)
    message = message[:coded_length]

    # Interleaver
    perm_key = np.random.permutation(coded_length)
    interleaved_message = message[perm_key]

    # Packet Generation
    preamble = np.zeros((preamble_length), dtype=int)  # Zeros for the preamble
    burst = np.concatenate((preamble, interleaved_message))

    # Spreading and Gold Code Multiplication
    up_burst = np.repeat(burst, spread_factor)
    chips_seq = np.bitwise_xor(up_burst, gold_code)  # XOR operation

    # Chips for the In-phase and Quadrature components
    chips_i = chips_seq[0::2]  # Elements at odd indices (In-phase)
    chips_q = chips_seq[1::2]  # Elements at even indices (Quadrature)

    # Symbols
    tx = modulate_qpsk(chips_i, chips_q)

    # Upsample and Filter the Signals
    txup = np.zeros(len(tx) * sps, dtype=complex)
    txup[::sps] = tx
    txsamples = np.convolve(txup, rrcfilter, mode="same")

    # Set signal power to the desired value
    txsamples = set_signal_power(txsamples, sig_power)

    # Preamble symbols
    preambleqpsk = modulate_qpsk(
        chips_i[:num_preamble_chips], chips_q[:num_preamble_chips]
    )

    return (
        txsamples,
        preambleqpsk,
        chips_seq,
        perm_key,
        payload_data,
        interleaved_message,
    )


def plot_spectrum(signal, fs):
    """
    Plot the spectrum of a signal.

    Parameters:
    signal (numpy array): The input signal.
    fs (float): The sampling frequency.

    Returns:
    None
    """
    # Compute the Power Spectral Density (PSD) using Welch's method
    frequencies, pxx_spec = welch(
        signal, fs, window="hann", nperseg=1024, scaling="spectrum"
    )

    # Convert PSD to dBm with a reference load of 1 ohm, Pxx has units of V**2
    pxx_spec_dbm = 10 * np.log10(pxx_spec * 1e3)

    # Plot the spectrum
    plt.figure(figsize=(10, 6))
    plt.plot(frequencies, pxx_spec_dbm, label="Samples")
    plt.title("Power")
    plt.xlabel("Frequency (Hz)")
    plt.ylabel("dBm")
    plt.legend()
    plt.grid(True)
    # plt.show(block=False)


def timing_estimation(
    input_samples, threshold, sps, preamble_symbols, detection_offset, starting_point
):
    """
    Timing estimation function for frame synchronization.

    Parameters:
    input_samples (numpy array): Input samples to be processed.
    threshold (float): Threshold for peak detection.
    sps (int): Samples per symbol.
    preamble_symbols (numpy array): Preamble symbols for correlation.
    detection_offset (int): Offset of the starting preamble symbol in the preamble series.
    starting_point (str): Starting point within the estimated starting symbol.

    Returns:
    int: Starting index for symbol synchronization.
    """
    decimated_sets = np.reshape(input_samples, (-1, sps)).T
    set_length = max(decimated_sets.shape)
    corr_sets = np.zeros((set_length + len(preamble_symbols) - 1, sps), dtype=complex)
    set_start_index = np.full(sps, -1, dtype=int)

    for k in range(sps):
        sig_set = decimated_sets[k, :].T
        sig_dim = max(sig_set.shape)
        pre_dim = max(preamble_symbols.shape)
        imp_resp = np.zeros((sig_dim + pre_dim - 1, 1), dtype=complex)
        sig_corr = np.zeros((sig_dim + pre_dim - 1, 1))

        # Create the zero-padded array
        padded_sig_set = np.concatenate(
            [
                np.zeros(pre_dim - 1),
                np.real(sig_set * np.conj(sig_set)),
                np.zeros(pre_dim - 1),
            ]
        )

        # Compute the moving sum
        mag_sig_set = (
            pd.Series(padded_sig_set)
            .rolling(window=pre_dim, center=False)
            .sum()
            .dropna()
            .values
        )

        corr_rslt = cross_correlation_calc(
            sig_set, preamble_symbols
        )  # Cross-correlation
        pre_power = np.real(np.sum(preamble_symbols.conj() * preamble_symbols))
        imp_resp = corr_rslt[sig_dim - pre_dim :] / pre_power
        sig_corr = np.abs(corr_rslt[sig_dim - pre_dim :]) / np.sqrt(
            mag_sig_set * pre_power
        )
        sig_corr[(mag_sig_set * pre_power) == 0] = 0  # Ensure normCorr is not undefined

        if np.any(sig_corr >= threshold):
            imp_resp_mag = np.real(imp_resp * imp_resp.conj())
            set_peak = np.argmax(imp_resp_mag)
            set_offset = set_peak - pre_dim
        else:
            set_offset = []

        corr_sets[:, k] = sig_corr
        if set_offset:  # Check if set_offset is not empty
            set_start_index[k] = set_offset - detection_offset + 1

    corr_sets_maxvals = np.max(np.abs(corr_sets), axis=0)
    corr_maxval = np.max(corr_sets_maxvals)
    maxval_set_idx = np.argmax(corr_sets_maxvals)
    corr_set_in_use = corr_sets[len(preamble_symbols) - 1 :, maxval_set_idx]

    if corr_maxval < 5.5 * np.mean(np.abs(corr_set_in_use)):
        starting_idx = []
    else:
        if np.any(set_start_index >= 0):
            set_peak_idx = set_start_index[maxval_set_idx]
            if starting_point == "Symbol beggining":
                starting_idx = max(
                    1, int((set_peak_idx) * sps + maxval_set_idx - (sps / 2))
                )
            else:  # starting_point == 'Symbol center'
                starting_idx = max(1, int((set_peak_idx) * sps + maxval_set_idx))
        else:
            starting_idx = []

    return starting_idx, corr_set_in_use


def cfo_correction(sig_in, fs, modulation_order, doppler, clk_missmatch, max_doppler):
    """
    Carrier Frequency Offset (CFO) correction function, 'FFT-Based' Algorithm.

    Parameters:
    sig_in (numpy array): Input signal to be processed.
    fs (int): Sampling frequency.
    modulation_order (int): Modulation order.
    doppler (float): Frequency offset due to Doppler shift.
    clk_missmatch (float): Frequency offset due to clock mismatch.

    Returns:
    numpy array: Signal after CFO correction.
    """
    starting_offset = 0
    fft_len = 2 ** int(np.ceil(np.log2(fs)))  # next power of 2
    sig_len = len(sig_in)
    coherent_sig_buffer = np.zeros(fft_len, dtype=complex)

    coherent_sig = sig_in**modulation_order

    if sig_len < fft_len:
        coherent_sig_buffer[: fft_len - sig_len] = coherent_sig_buffer[sig_len:]
        coherent_sig_buffer[fft_len - sig_len :] = coherent_sig
        sig_pow = np.abs(np.fft.fft(coherent_sig_buffer, fft_len))
    elif sig_len == fft_len:
        sig_pow = np.abs(np.fft.fft(coherent_sig, fft_len))
    else:  # sig_len > fft_len
        ffts = int(np.ceil(sig_len / fft_len))
        sig_pow = np.zeros(fft_len, dtype=coherent_sig.dtype)
        for fft_idx in range(ffts - 1):
            new_coherent_sig = coherent_sig[
                (fft_idx * fft_len) : (fft_idx * fft_len + fft_len)
            ]
            sig_pow += np.abs(np.fft.fft(new_coherent_sig, fft_len))
        sig_pow += np.abs(np.fft.fft(coherent_sig[-fft_len:], fft_len))

    # Find the frequency offset
    spectrum = np.fft.fftshift(sig_pow)
    df = fs / fft_len
    min_spect_idx = int(np.floor((fs / 2 - modulation_order * max_doppler) / df))
    max_spect_idx = int(np.ceil((fs / 2 + modulation_order * max_doppler) / df))
    # Select the spectrum around the maximum and minimum Doppler frequencies
    spectrum1 = spectrum[min_spect_idx : max_spect_idx + 1]

    plt.figure(figsize=(10, 6))
    plt.plot(spectrum)
    plt.grid(True)
    plt.title("CFO Estimation with FFT peak")
    plt.show(block=False)

    max_val_index1 = np.argmax(spectrum1)
    max_val_index = min_spect_idx + max_val_index1
    folded_idx = max_val_index - fft_len // 2 + 1  # -fs/2 : fs/2
    seg_freq = fs / fft_len
    cfo_est = seg_freq * (folded_idx - 1) / modulation_order

    cfo_step_vector = cfo_est * np.arange(len(sig_in)) * (1 / fs)
    # Frequency correction
    sig_out = sig_in * np.exp(
        1j * 2 * np.pi * (starting_offset - cfo_step_vector[: len(sig_in)])
    )

    print(f"Real frequency offset = {doppler + clk_missmatch:.6f} Hz")
    print(f"Estimated coarse frequency offset = {cfo_est / 1000:.3f} kHz")
    print(f"Estimation error = {cfo_est - (doppler + clk_missmatch):.3f} Hz")

    return sig_out


def timing_recovery(data_in_ss1, zeta, bnts, kp, k0, sps, num_preamble_chips):
    """
    Timing recovery function for symbol synchronization.

    Parameters:
    data_in_ss1 (numpy array): Input data to be processed.
    zeta (float): Damping factor of the loop filter.
    bnts (float): Normalized loop bandwidth.
    kp (float): Proportional gain of the loop filter.
    k0 (float): Constant multiplier.
    sps (int): Samples per symbol.
    num_preamble_chips (int): Number of preamble chips.

    Returns:
    numpy array: Symbols after timing recovery.
    """
    alpha = 0.5
    max_out_len_factors = [11, 10]
    frame_len = len(data_in_ss1)
    max_out_len = int(
        np.ceil(frame_len * max_out_len_factors[0] / (max_out_len_factors[1] * sps))
    )
    timing_error_detector_type = "Gardner (non-data-aided)"
    theta = bnts / sps / (zeta + 0.25 / zeta)
    d = (1 + 2 * zeta * theta + theta**2) * k0 * kp
    propotional_gain = (4 * zeta * theta) / d
    intergrator_gain = (4 * theta**2) / d

    # Filter coefficients
    interpolation_filter = np.array(
        [
            [0, 0, 1, 0],  # Constant
            [-alpha, 1 + alpha, -(1 - alpha), -alpha],  # Linear
            [alpha, -alpha, -alpha, alpha],  # Quadratic
        ]
    )

    # PLL properties
    strobe = False
    sum_strobes = 0
    filt_state = 0.0
    fract_interp_interval = 0.0
    nco_count = 0.0
    previous_strobes = np.zeros(sps, dtype=bool)
    timing_error = np.zeros(frame_len)
    interpolation_filter_state = np.zeros(3, dtype=complex)
    ted_set = np.zeros(sps, dtype=complex)
    symbol_buffer = np.zeros(max_out_len, dtype=complex)
    symbol_buffer_len = len(symbol_buffer)
    overflow = False

    for sample in range(frame_len):
        # Stop processing if an overflow occurs
        if sum_strobes == symbol_buffer_len and strobe:
            overflow = True
            break

        sum_strobes += strobe
        timing_error[sample] = fract_interp_interval

        # Parabolic interpolator
        data_set_in = np.concatenate(
            ([data_in_ss1[sample]], interpolation_filter_state)
        )
        ss1_symbol_out = np.sum(
            (interpolation_filter @ data_set_in)
            * np.array([1, fract_interp_interval, fract_interp_interval**2])
        )
        interpolation_filter_state = data_set_in[:3]

        if strobe:
            symbol_buffer[sum_strobes - 1] = ss1_symbol_out

        if sum_strobes > symbol_buffer_len:
            overflow = True
            break

        # Timing error detector (TED)
        err = 0.0
        if timing_error_detector_type == "Gardner (non-data-aided)":
            if strobe and all([not s for s in previous_strobes[1:]]):
                sample1 = ted_set[len(ted_set) // 2 - (sps % 2)]
                sample2 = ted_set[len(ted_set) // 2]
                mid_sample_val = (sample1 + sample2) / 2
                err = (
                    mid_sample_val.real * (ted_set[0].real - ss1_symbol_out.real)
                ) + (mid_sample_val.imag * (ted_set[0].imag - ss1_symbol_out.imag))
            else:
                err = 0.0

        # Update the TED buffer
        strobe_sum = sum(np.append(previous_strobes[1:], [strobe]))
        if strobe_sum == 0:
            pass
        elif strobe_sum == 1:
            ted_set = np.append(ted_set[1:], [ss1_symbol_out])
        else:  # strobe_sum > 1
            ted_set = np.append(ted_set[2:], [0, ss1_symbol_out])

        loop_filt_out = err * intergrator_gain + filt_state
        correction = err * propotional_gain + loop_filt_out
        filt_state = loop_filt_out

        # Update strobe and fractional interpolation interval
        st = correction + 1 / sps
        previous_strobes = np.append(previous_strobes[1:], [strobe])
        strobe = nco_count < st
        if strobe:
            fract_interp_interval = nco_count / st
        nco_count = (nco_count - st) % 1

    symbols_out = symbol_buffer[:sum_strobes]

    if overflow:
        message = f"Symbol Dropping: {frame_len - sample}, {max_out_len}"
        raise ValueError(message)

    # Plot the timing error
    plt.figure(figsize=(10, 6))
    plt.plot((timing_error - 0.5) - np.sign(timing_error - 0.5) * 0.5)
    plt.title("Timing Error")
    plt.xlabel("Sample Number")
    plt.ylabel("Timing Error (Samples)")
    plt.show(block=False)

    # Scatter plot for payload constellation
    plt.figure(figsize=(6, 6))
    plt.scatter(
        symbols_out[num_preamble_chips:].real, symbols_out[num_preamble_chips:].imag
    )
    plt.title("Payload Constellation")
    plt.xlabel("In-phase")
    plt.ylabel("Quadrature")
    plt.grid(True)
    plt.show(block=False)

    return symbols_out


def phase_ambiguity_resolution(
    symbols_in, preamb_chips_used, m_qam, num_preamble_chips, num_burst_chips
):
    """
    Resolve the phase ambiguity in the received symbols.

    Parameters:
    symbols_in (numpy array): Received symbols.
    preamb_chips_used (int): The preamble chips.
    m_qam (int): Modulation order.
    num_preamble_chips (int): Number of preamble chips.
    num_burst_chips (int): Number of chips in the packet.

    Returns:
    numpy array: Bitstream after phase ambiguity resolution.
    """
    preamble_symbols = preamb_chips_used - 0.5
    table_crosscorr = np.zeros(m_qam * 2, dtype=float)
    table_idx = np.zeros(m_qam * 2, dtype=int)
    table_p = np.zeros(m_qam * 2, dtype=int)
    table_k = np.zeros(m_qam * 2, dtype=int)
    pr_counter = 0

    for p1 in range(2):
        for l1 in range(m_qam):
            out_sig = demodulate_qpsk(symbols_in * np.exp(1j * l1 * np.pi / 2))
            received_ci = out_sig[0::2]
            received_cq = out_sig[1::2]

            received_ci_p = received_ci[p1 : (num_preamble_chips + 10) + p1]
            received_cq_p = received_cq[: (num_preamble_chips + 10)]
            received_bits = np.vstack((received_ci_p, received_cq_p)).T.flatten() - 0.5

            prb_preamble_symbols = np.flip(preamble_symbols, axis=0)
            filt_states = np.zeros((len(prb_preamble_symbols) - 1), dtype=float)
            # Apply the filter
            cross_corr_f, new_filt_states = lfilter(
                prb_preamble_symbols, 1, received_bits, zi=filt_states
            )
            filt_states = new_filt_states
            peak_idx = np.where(cross_corr_f == np.max(cross_corr_f))[0][0]

            table_crosscorr[pr_counter] = np.max(cross_corr_f)
            table_idx[pr_counter] = peak_idx
            table_p[pr_counter] = p1
            table_k[pr_counter] = l1
            pr_counter += 1

    indices = np.where(table_crosscorr == np.max(table_crosscorr))[0]
    indices = indices[0]
    p_idx1 = table_idx[indices]
    p2 = table_p[indices]
    l2 = table_k[indices]

    # I and Q indices
    actual_out_sig = demodulate_qpsk(symbols_in * np.exp(1j * l2 * np.pi / 2))
    preambleteststartidx = int(p_idx1 - num_preamble_chips * 2 + 1)
    i_idx = int(preambleteststartidx + 2 * p2)
    q_idx = int(preambleteststartidx + 1)

    # Extract bitstream
    final_ci1 = actual_out_sig[i_idx : i_idx + 2 * num_burst_chips : 2]
    final_cq1 = actual_out_sig[q_idx : q_idx + 2 * num_burst_chips : 2]
    bitstream_out = np.vstack((final_ci1, final_cq1)).T.flatten()

    return bitstream_out


def generate_jamming_signal(jam_bw, jam_fs, num_samples, des_power, fj):
    """
    Generate a jamming signal with the specified parameters.

    Parameters:
    jam_bw (float): Jamming signal bandwidth.
    jam_fs (float): Sampling frequency for the jamming signal.
    num_samples (int): Number of burst samples.
    des_power (float): Transmission power in dBm.
    fj (float): Jam signal offset frequency.

    Returns:
    numpy array: The generated jamming signal.
    """
    # Jamming Signal
    interp_rate = 16
    jam_rolloff = 0.5
    jam_fs1 = jam_fs / interp_rate
    jam_sps = (1 + jam_rolloff) * jam_fs1 / jam_bw
    jam_sps = round(jam_sps / 2) * 2
    num_jam_bits = 2 * np.floor((num_samples / interp_rate) / jam_sps).astype(int)
    jam_bits = np.random.randint(0, 2, num_jam_bits)
    jam_symbols = modulate_qpsk(jam_bits[0::2], jam_bits[1::2])
    jam_num_taps1 = int(3 * jam_sps * jam_sps)
    jt1, jam_filter1 = rrcosfilter(jam_num_taps1, jam_rolloff, jam_sps, 1)
    jam_filter1 = jam_filter1 / np.sum(jam_filter1)
    jam_symbols_up = np.zeros(len(jam_symbols) * jam_sps, dtype=complex)
    jam_symbols_up[::jam_sps] = jam_symbols
    jam_signal1 = np.convolve(jam_symbols_up, jam_filter1, mode="full")
    jam_signal1 = jam_signal1[
        int(jam_num_taps1 / 2) : int(len(jam_signal1) - 1 - jam_num_taps1 / 2)
    ]
    jam_num_taps2 = int(3 * interp_rate * interp_rate)
    jt2, jam_filter2 = rrcosfilter(jam_num_taps2, jam_rolloff, interp_rate, 1)
    jam_filter2 = jam_filter2 / np.sum(jam_filter2)
    jam_symbols_interp = np.zeros(len(jam_signal1) * interp_rate, dtype=complex)
    jam_symbols_interp[::interp_rate] = jam_signal1
    jam_signal2 = np.convolve(jam_symbols_interp, jam_filter2, mode="full")
    jam_signal2 = jam_signal2[
        int(jam_num_taps2 / 2) : int(len(jam_signal2) - 1 - jam_num_taps2 / 2)
    ]
    jam_zeros = np.zeros(num_samples - len(jam_signal2), dtype=jam_signal2.dtype)
    jam_signal2 = np.concatenate((jam_signal2, jam_zeros))
    jam_signal2 = set_signal_power(jam_signal2, des_power)
    # Apply the frequency offset
    tj = np.arange(len(jam_signal2))
    jam_signal = jam_signal2 * np.exp(1j * 2 * np.pi * fj * tj / jam_fs)

    return jam_signal


# Main Code Execution
os.system("cls")  # clear the console screen
np.random.seed(4)  # set a random seed (Mersenne Twister algorithm) for repeatability

# Adaptable parameters
USERS_NUM = 3  # number of users
BIT_RATE = 9600  # bits per second
VELOCITY = -7000  # relative velocity between transmitter and receiver in m/s
TX_POWER = 30  # transmit power in dBm (33 dBm max)
MAX_POSSIBLE_DOPPLER = 5000  # maximum detected Doppler in Hz
JAM_SIGNAL_ENABLED = True  # enable or not the jamming signal
CHOOSE_TRANSMITTER = 3  # choose the transmitted signal to be decoded
SNR = -2  # signal-to-noise ratio in dB

# System parameters
ROLLOFF = 1  # Filter rolloff factor
FILTLEN = 20  # Filter length in symbols
SPS = 8  # define samples per symbol
NUM_TAPS = FILTLEN * SPS  # Number of taps (even number)

# Convolutional 1/2 code
# Define the trellis
CONSTRAINT_LENGTH = 3
trellis = Trellis(np.array([3]), np.array([[0o3, 0o7]]))  # octal representation
CODERATE = trellis.k / trellis.n

PAYLOAD_LENGTH = 247  # payload length in bits
CODED_LENGTH = int(
    PAYLOAD_LENGTH * 2 + CONSTRAINT_LENGTH / CODERATE
)  # coded length in bits
PREAMBLE_LENGTH = 512 - CODED_LENGTH  # preamble length in bits
SPREADING_FACTOR = 64  # DSSS spreading factor
M_QAM = 4  # QPSK

CODED_BITS_RATE = BIT_RATE / (1 / 2)  # coded bits per second
CHIP_RATE = CODED_BITS_RATE * SPREADING_FACTOR  # chips per second
BAUD_RATE = CHIP_RATE / math.log2(M_QAM)  # symbols per second
BW = (1 + ROLLOFF) * BAUD_RATE  # Bandwidth
FS = BAUD_RATE * SPS  # sampling frequency
FC = 60e6  # carrier frequency in MHz

DISTANCE = 300000  # distance of transmitter and receiver in meters
TIMES_SIGNAL_DURATION = 5  # length of simulation in seconds, integer number
TIMES_SIGNAL_CATCH = 0.2  # number of seconds until burst is received

PACKET_LENGTH = PREAMBLE_LENGTH + CODED_LENGTH  # packet length in bits
IQ_SPREADING = int(SPREADING_FACTOR / 2)  # Spreading factor for I and Q components
NUM_BURST_CHIPS = IQ_SPREADING * PACKET_LENGTH  # number of chips in the packet
NUM_PREAMBLE_CHIPS = PREAMBLE_LENGTH * IQ_SPREADING
NUM_BURST_SAMPLES = int((PACKET_LENGTH / 2) * SPREADING_FACTOR * SPS)

# RRC filter generation
t, txrrcFilter = rrcosfilter(NUM_TAPS, ROLLOFF, SPS, 1)  # Generate the RRC filter
txrrcFilter = txrrcFilter / np.sum(
    txrrcFilter
)  # The linear filter gain must be equal to 1

# Plot the RRC filter
plt.figure(figsize=(10, 6))
plt.plot(t, txrrcFilter)
plt.title("Raised Cosine Filter")
plt.xlabel("Time")
plt.ylabel("Amplitude")
plt.grid(True)
# plt.show(block=False)

# Gold Codes
# Define the polynomial coefficients and initial conditions
NF = 15
poly_coeffs1 = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1]
poly_coeffs2 = [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1]
initial_conditions1 = [1] + [0] * (NF - 1)
initial_conditions2 = [1] + [0] * (NF - 1)
FRAME_LENGTH = 2**NF - 1

gold_code_seqs = generate_gold_codes(
    poly_coeffs1,
    initial_conditions1,
    poly_coeffs2,
    initial_conditions2,
    FRAME_LENGTH,
    100,
)

gold_code_set = np.zeros((gold_code_seqs.shape[0], USERS_NUM), dtype=int)
GOLD_CODE_INIT_NUM = 5
for ii in range(USERS_NUM):
    gold_code_num = GOLD_CODE_INIT_NUM + (ii * 10)
    gold_code_set[:, ii] = gold_code_seqs[:, gold_code_num]  # ith gold code

# Users Signals
txSamples = np.zeros((NUM_BURST_SAMPLES, USERS_NUM), dtype=complex)
preambleQPSK = np.zeros((NUM_PREAMBLE_CHIPS, USERS_NUM), dtype=complex)
chips = np.zeros((2 * NUM_BURST_CHIPS, USERS_NUM), dtype=int)
permutation_key = np.zeros((CODED_LENGTH, USERS_NUM), dtype=int)
payload = np.zeros((PAYLOAD_LENGTH, USERS_NUM), dtype=int)
rearranged_message = np.zeros((CODED_LENGTH, USERS_NUM), dtype=int)
for jj in range(USERS_NUM):
    users_signals = create_user_signal(
        PAYLOAD_LENGTH,
        trellis,
        CODED_LENGTH,
        PREAMBLE_LENGTH,
        SPREADING_FACTOR,
        gold_code_set[:, jj],
        txrrcFilter,
        TX_POWER,
        NUM_PREAMBLE_CHIPS,
        SPS,
    )

    txSamples[:, jj] = users_signals[0]
    preambleQPSK[:, jj] = users_signals[1]
    chips[:, jj] = users_signals[2]
    permutation_key[:, jj] = users_signals[3]
    payload[:, jj] = users_signals[4]
    rearranged_message[:, jj] = users_signals[5]

# Spectrum Analysis
plot_spectrum(txSamples[:, 0], FS)


# Channel and Receiver Impairments

# Jamming Signal
if JAM_SIGNAL_ENABLED:
    JAM_BW = 25e3
    JAM_FS = FS
    FJ = 300e3  # Jam signal offset
    JAMMING_POWER = 33  # dBm
    jam_s = generate_jamming_signal(
        JAM_BW, JAM_FS, NUM_BURST_SAMPLES, JAMMING_POWER, FJ
    )
    plot_spectrum(jam_s + txSamples[:, 0], FS)


DOPPLER_OFFSET = (FC * VELOCITY) / speed_of_light
CLK_OFFSET = -600  # 10 ppm oscillator mismatch
ACUMULATED_FREQ_OFFSET = DOPPLER_OFFSET + CLK_OFFSET

# # Free space path loss and noise floor calculation
# NOISE_FLOOR_POWER = -174 + 10 * np.log10(2 * CHIP_RATE) # Noise floor (dBm)
# PATHLOSS = 4 * np.pi * DISTANCE / (speed_of_light / FC) # Free space path loss
# PATHLOSS = max(PATHLOSS, 1)
# PATHLOSS = 20 * np.log10(PATHLOSS)
# SNR = (TX_POWER - PATHLOSS) - NOISE_FLOOR_POWER

# SNR calculation according to AMSAT-IARU Link model (excel)
SIGNAL_POWER_AT_SPACECRAFT_LNA_INPUT = -145.5  # dBW
SPACECRAFT_RECEIVED_BANDWIDTH = 1.25e3  # Hz
SPACECRAFT_RECEIVED_NOISE_POWER = -143.5  # dBW
# SNR = -2

# Empty samples before the transmission appears
SIGNAL_TIME_OFFSET = floor(TIMES_SIGNAL_CATCH * NUM_BURST_SAMPLES)
empty_set = np.zeros(floor(TIMES_SIGNAL_DURATION * NUM_BURST_SAMPLES), dtype=complex)

# Initialize the received data buffers
sample_set = np.zeros((NUM_BURST_SAMPLES, USERS_NUM), dtype=complex)
rec_data_set = np.zeros((len(empty_set), USERS_NUM), dtype=complex)
awgn_set = np.zeros((len(empty_set), USERS_NUM), dtype=complex)
for jj in range(USERS_NUM):
    # Add frequency offset to the received signal
    sample_set[:, jj] = add_frequency_offset(
        txSamples[:, jj], FS, ACUMULATED_FREQ_OFFSET
    )
    rec_data_set[SIGNAL_TIME_OFFSET : SIGNAL_TIME_OFFSET + NUM_BURST_SAMPLES, jj] = (
        sample_set[:, jj]
    )
    # Add AWGN to the buffers
    awgn_set[:, jj] = add_awgn_noise(rec_data_set[:, jj], SNR)

if JAM_SIGNAL_ENABLED:
    # Set the buffer of the jamming signal
    rec_data_set_j = np.zeros((len(empty_set)), dtype=complex)
    rec_data_set_j[SIGNAL_TIME_OFFSET : SIGNAL_TIME_OFFSET + NUM_BURST_SAMPLES] = jam_s
    awgn_set_j = add_awgn_noise(rec_data_set_j, SNR)

if JAM_SIGNAL_ENABLED:
    comb_data = np.sum(awgn_set, axis=1) + awgn_set_j
else:  # No jamming signal
    comb_data = np.sum(awgn_set, axis=1)

plt.figure(figsize=(10, 6))
plt.subplot(311)
plt.plot(
    np.linspace(1 / FS, len(awgn_set[:, 0]) / FS, len(awgn_set[:, 0])),
    np.real(awgn_set[:, 0]),
)
plt.title("Buffer 1")
plt.subplot(312)
plt.plot(
    np.linspace(1 / FS, len(awgn_set[:, 1]) / FS, len(awgn_set[:, 1])),
    np.real(awgn_set[:, 1]),
)
plt.title("Buffer 2")
plt.subplot(313)
plt.plot(np.linspace(1 / FS, len(comb_data) / FS, len(comb_data)), np.real(comb_data))
plt.title("Buffer 1 + 2")
plt.tight_layout()  # Adjust subplots to fit into figure area.
# plt.show(block=False)

# Receiver
preambleQPSK_used = preambleQPSK[:, CHOOSE_TRANSMITTER - 1]
preamble_chips_used = chips[: NUM_PREAMBLE_CHIPS * 2, CHOOSE_TRANSMITTER - 1]
gold_code_set_used = gold_code_set[:, CHOOSE_TRANSMITTER - 1]
permutation_key_used = permutation_key[:, CHOOSE_TRANSMITTER - 1]
payload_used = payload[:, CHOOSE_TRANSMITTER - 1]
rearranged_message_used = rearranged_message[:, CHOOSE_TRANSMITTER - 1]


BUFFERS = floor(len(comb_data) / NUM_BURST_SAMPLES)
# Initialize an empty buffer
# samples_in = (np.random.randn(NUM_BURST_SAMPLES*2) + 1j * np.random.randn(NUM_BURST_SAMPLES*2))
samples_in = np.zeros((NUM_BURST_SAMPLES * 2), dtype=complex)

# Buffer Scan
for N in range(1, BUFFERS + 1):
    NEXT_IDX = (N - 1) * NUM_BURST_SAMPLES

    if N == 1:
        samples_in[:NUM_BURST_SAMPLES] = samples_in[NUM_BURST_SAMPLES:]
        samples_in[NUM_BURST_SAMPLES:] = comb_data[
            NEXT_IDX : NEXT_IDX + NUM_BURST_SAMPLES
        ]
    else:
        samples_in = comb_data[
            NEXT_IDX - NUM_BURST_SAMPLES : NEXT_IDX + NUM_BURST_SAMPLES
        ]

    # AGC
    DESIRED_POWER_1 = 1
    agc_samples_in = change_average_power(samples_in, DESIRED_POWER_1)

    # Coarse Timing Estimation
    DETECTION_START_IN_PREAMBLE1 = 40
    DETECTION_LENGTH_IN_PREAMBLE = 300
    preamble_Chips1 = preambleQPSK_used[
        DETECTION_START_IN_PREAMBLE1 : DETECTION_START_IN_PREAMBLE1
        + DETECTION_LENGTH_IN_PREAMBLE
    ]

    for FREQ_OFFSET_VAL in np.arange(
        -MAX_POSSIBLE_DOPPLER, MAX_POSSIBLE_DOPPLER + 100, 100
    ):
        preamble_with_cfo = add_frequency_offset(
            preamble_Chips1, BAUD_RATE, FREQ_OFFSET_VAL
        )
        THRESHOLD1 = 0.30
        STARTING_POINT1 = "Symbol beggining"
        idx1, correlated_samples1 = timing_estimation(
            agc_samples_in,
            THRESHOLD1,
            SPS,
            preamble_with_cfo,
            DETECTION_START_IN_PREAMBLE1,
            STARTING_POINT1,
        )

        if idx1 and (idx1 < NUM_BURST_SAMPLES):
            print(f"Found preamble at sample index {idx1}")
            print(
                f"Found preamble after searching at frequency {FREQ_OFFSET_VAL:.0f} Hz"
            )
            break  # break out of frequency search loop

    if not idx1:
        print(f"Preamble not found after {(N * NUM_BURST_SAMPLES) / FS} seconds")
    else:
        break

if not idx1:
    raise ValueError("Preamble not detected amongst simulation samples.")

plt.figure(figsize=(10, 6))
plt.plot(np.abs(correlated_samples1))
plt.grid(True)
plt.title("Correlator Output Buffer")
plt.xlabel("Correlator Lag")
# plt.show(block=False)

# Extract the signal
estimated_signal = agc_samples_in[idx1 : idx1 + ((NUM_BURST_CHIPS + 20) * SPS)]


# Estimate and correct carrier offset 'FFT-Based' Algorithm
coarseSyncOut = cfo_correction(
    estimated_signal, FS, M_QAM, DOPPLER_OFFSET, CLK_OFFSET, MAX_POSSIBLE_DOPPLER
)


# Matched filter
syncOut = np.convolve(coarseSyncOut, txrrcFilter, mode="same")


# Timing Estimation
DETECTION_START_IN_PREAMBLE2 = 40
PREAMBLE_SET = preambleQPSK_used[DETECTION_START_IN_PREAMBLE2:]
THRESHOLD2 = 0.30
STARTING_POINT2 = "Symbol center"
idx2, correlated_samples2 = timing_estimation(
    syncOut,
    THRESHOLD2,
    SPS,
    PREAMBLE_SET,
    DETECTION_START_IN_PREAMBLE2,
    STARTING_POINT2,
)

if not idx2:
    raise ValueError("Preamble lost")


plt.figure(figsize=(10, 6))
plt.plot(np.abs(correlated_samples2))
plt.grid(True)
plt.title("Correlator Output Buffer")
plt.xlabel("Correlator Lag")
# plt.show(block=False)

# Timing Recovery
ZETA = 2
BNTS = 0.005
KP = 3
K0 = -1
synced_qpsk = timing_recovery(
    syncOut[idx2:], ZETA, BNTS, KP, K0, SPS, NUM_PREAMBLE_CHIPS
)

# Phase ambiguity resolution
final_stream = phase_ambiguity_resolution(
    synced_qpsk, preamble_chips_used, M_QAM, NUM_PREAMBLE_CHIPS, NUM_BURST_CHIPS
)

# DSSS Despreading
Fbdn = np.bitwise_xor(
    final_stream[NUM_PREAMBLE_CHIPS * 2 :], gold_code_set_used[NUM_PREAMBLE_CHIPS * 2 :]
)
Fchips = Fbdn.reshape((-1, SPREADING_FACTOR)).T

# ML decision
THRESHOLD_DESREADING = SPREADING_FACTOR / 2
despread_message = np.sum(Fchips, axis=0) > THRESHOLD_DESREADING


# Check coded data mistakes
CODED_DATA_CORRECTS = np.sum(despread_message == rearranged_message_used)
print(f"{CODED_LENGTH - CODED_DATA_CORRECTS} errors detected in the coded message")

# Deinterleaver
rearranged_payload = np.zeros(CODED_LENGTH, dtype=int)
rearranged_payload[permutation_key_used] = despread_message

# Convolutional (Viterbi Decoding) 1/2
TB_DEPTH = 5 * (CONSTRAINT_LENGTH + 1)
dec_payload = viterbi_decode(
    rearranged_payload, trellis, tb_depth=None, decoding_type="hard"
)
dec_payload = dec_payload[:PAYLOAD_LENGTH]

# BER
errs = np.sum(dec_payload != payload_used)
ber = errs / len(dec_payload)

# Print the results
if errs > 0:
    print(f"{errs} Error detected ")
else:
    print("No errors detected")

plt.show()

# End of satcom_final.py module
